#jquery-ptemplatejs
jquery的pTemplateJs插件

# 例子
```
<div p-template="test">
  <h1>{{title}}</h1>
  <a href="#" p-handle:click="handleAClick">{{title}}</a>
  <a href="#" p-router:href="/test?a=1">{{title}}1</a>
  <input type="text" value="{{title}}" p-handle:change="handleInputChange" />
</div>

jQuery.router({
  "/test": function(e, args){
    console.log(args)
  }
}).renderDom("test", {
  title: "test",
  handle: {
    handleAClick: function(e) {
      e.preventDefault();
      console.log(e)
      jQuery.updateDom("test", {
        title: "test_" + 1
      })
    },
    handleInputChange: function(e) {
      jQuery.updateDom("test", {
        title: this.value
      })
    }
  }
}, function() {
   console.log("end")
});
```

# 方法及说明
```
$.renderDom 渲染标签
$.updateDom 更新标签
$.createTemplate 创建模板
$.cloneTemplate 克隆模板
$.router 路由
$.tmplattr 模板伪类属性库，可以使用$.extend扩展
$.routers 路由库，可以使用$.extend扩展
$.tmpl 模板赋值
$.createDom 创建标签
$.setBaseFontSize 设置页面默认字体大小
```

# 使用
```
$.renderDom(
selector, 模板名 
data, 数据
parent, 插入的父标签,不添加将渲染模板标签自身
callback) 回调

$.updateDom(
selector, 模板名称
data, 数据
callback) 回调

$.cloneTemplate(
name1, 原模板名称 
name2) 新模板名称

$.createTemplate(
selector 模板名称
)

$.router(
args 路由对象或路由地址
)

$.createDom(
tagName, 标签TAGNAME
attrs, 标签属性对象
children) 子节点集

$.tmpl(
element, 标签
data) 数据

$.setBaseFontSize(
size 数字字号
)
```